import React, {useState} from 'react';
import {Button, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import axios from 'axios';
import { useRouter } from 'next/router'


// import "./Login.css";

const Login = (props) => {
  const [email, setEmail] = useState('');
  const router = useRouter()

  const [password, setPassword] = useState('');

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

  const handleapi = () => {
    axios
      .post('https://devapi.instantanalytics.io/api/v1/login', {
        emailId: email,
        password: password,
      })
      .then(function (response) {
        if(response && response.data && response.data.response && response.data.response.accessToken){
          router.push("about")
        }
        else if (response && response.data && response.data.message){
alert(response.data.message)
        }
      });
  };

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          {/* <ControlLabel>Email</ControlLabel> */}
          <p>Email</p>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          {/* <ControlLabel>Password</ControlLabel> */}
          <p>password</p>
          <FormControl
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button
          onClick={handleapi}
          disabled={!validateForm()}
          type="submit"
          style={{
            backgroundColor: validateForm() ? 'green' : 'red',
            color: 'white',
            marginTop: '10px',
          }}
        >
          Login
        </Button>
      </form>
    </div>
  );
};

export default Login;
